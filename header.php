<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package macbeth-roofing
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php get_template_part( '/inc/schema' ); ?>
	<?php wp_head(); ?>

	<script async src='https://tag.simpli.fi/sifitag/8f5cfcf0-23f4-0136-46cf-067f653fa718'></script>

	<!-- Google Code for Remarketing Tag -->
	<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1008090926;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1008090926/?guid=ON&amp;script=0"/>
	</div>
	</noscript>
</head>
<body <?php body_class(); ?>>

<?php 
	//Thank you Page
	if(get_the_ID() == 1813){
		get_template_part("/inc/google-conversion");
	}
?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'macbeth-roofing' ); ?></a>

	<header id="masthead" class="site-header">
		<?php  get_template_part("/inc/utility-bar"); ?>
		<div class="site-branding">
			<?php  the_custom_logo(); ?>
		</div><!-- .site-branding -->
		<?php  get_template_part("/inc/nav-main"); ?>
	</header><!-- #masthead -->

	<?php 
		
		if(!is_front_page()) {
			get_template_part('inc/featured-image');
		} else {
			get_template_part('inc/slick-home'); 
		} 

	?>

	<?php if (!is_front_page() && !is_page( 'request-a-quote' )) : ?>
		<div id="content" class="site-content">
		<?php elseif (is_page( 'request-a-quote' )) : ?>
			<div>
	<?php endif; ?>
