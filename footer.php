<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package macbeth-roofing
 */

?>
	<?php if (!is_front_page() && !is_page( 'request-a-quote' )) : ?>
		</div><!-- #content -->
		<?php elseif (is_page( 'request-a-quote' )) : ?>
			</div>
		<?php endelseif; ?>
	<?php endif; ?>

	<footer id="colophon" class="site-footer">
		
		<?php get_template_part("/inc/cta-request-quote-banner"); ?>

		<div class="bg-gray-base">
			<div class="container pt-lg pb-lg">
				
				<div class="site-info">
					<?php get_template_part("/inc/companyinfo"); ?>
				</div>

				<section class="blog-feed">
					<h3 class="h4 text-uppercase">Our Latest Blog Posts</h3>
					<?php get_template_part("/inc/feed-blogpost"); ?>
				</section>

				<section class="contact-lead">
					<div class="box-right">
						<h3 class="h4 text-uppercase">Contact Us</h3>
						<?php get_template_part("/inc/address-card"); ?>
						
						<h3 class="h4 text-uppercase">Keep in touch</h3>
						<div class="text-center"><?php get_template_part("/inc/social-links"); ?></div>
					</div>
				</section>

				<section class="contact-location text-center">
					<h3 class="h4 text-uppercase text-center">Residential Commercial and Industrial Roofers</h3>
					<?php get_template_part("/inc/nav-footer-locations"); ?>
				</section>
			</div>
		</div>

		<div class="nav-footerutility bg-gray pt-sm"><?php get_template_part("/inc/nav-footerutility"); ?></div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php get_template_part("/inc/contact-us-modal"); ?>
<?php wp_footer(); ?>

</body>
</html>
