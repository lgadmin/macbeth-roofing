<?php
/**
 * macbeth-roofing functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package macbeth-roofing
 */

if ( ! function_exists( 'macbeth_roofing_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function macbeth_roofing_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on macbeth-roofing, use a find and replace
		 * to change 'macbeth-roofing' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'macbeth-roofing', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

	// Menus
	function stonealpha_menus() {
	  register_nav_menus(
	    array(
	      'primary-menu-general' => __( 'Primary' ),
	      'primary-menu-commercial' => __( 'Primary-Commercial' ),
	      'primary-menu-residential' => __( 'Primary-Residential' ),
	      'footer-menu' => __( 'Footer' ),
	      'footer-menu-utility' => __( 'Footer Utility' ),
	      'footer-menu-locations' => __( 'Footer Locations' )
	    )
	  );
	}
	add_action( 'init', 'stonealpha_menus' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'macbeth_roofing_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'macbeth_roofing_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function macbeth_roofing_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'macbeth_roofing_content_width', 640 );
}
add_action( 'after_setup_theme', 'macbeth_roofing_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function macbeth_roofing_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'macbeth-roofing' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'macbeth-roofing' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'macbeth_roofing_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function macbeth_roofing_scripts() {
	
	wp_enqueue_style( 'macbeth-roofing-style', get_stylesheet_uri() );
	
	// Fonts
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', false );
	
	// Register Scripts
	wp_register_script( 'bootstrapjs', get_template_directory_uri() . "/js/bootstrap.min.js", array('jquery'), '3.3.7', true );
	wp_register_script( 'macbeth-roofing-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_register_script( 'lg-scripts', get_template_directory_uri() . "/js/lg-scripts.js", array('jquery'), false, true );
	wp_register_script( 'slick-carousel', get_template_directory_uri() . "/js/slick.min.js", array('jquery'), '1.6', true );
	wp_register_script( 'lg-ga', get_template_directory_uri() . "/js/lg-ga.js", array(), false, true );


	// Enqueue scripts. Only load what script(s) are needed. Conditional statement if wish.
	wp_enqueue_script( 'macbeth-roofing-skip-link-focus-fix' );
	wp_enqueue_script( 'bootstrapjs' );
	wp_enqueue_script( 'lg-scripts' );
	wp_enqueue_script( 'slick-carousel' );
	wp_enqueue_script( 'lg-ga' );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {wp_enqueue_script( 'comment-reply' ); }
}
add_action( 'wp_enqueue_scripts', 'macbeth_roofing_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load Custom Post Types
 */
	include get_template_directory() . '/LG-portfolio/main.php';

// Register Custom Navigation Walker
require_once(get_template_directory() . '/inc/wp-bootstrap-navwalker.php');

