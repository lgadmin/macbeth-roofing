<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
      

    <div class="modal-content">


      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title h5" id="myModalLabel">Ask for a quote</h4>
      </div>

      <div class="modal-body">
        <?php gravity_form( 2, false, false, false, '', false ); ?>
      </div>
    </div>
  </div>
</div>