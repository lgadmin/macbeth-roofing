<?php 
	$content          = get_sub_field('content');
	$background_color = get_sub_field('background_color');
?>

<div class="flexible-content pt-lg pb-lg <?php if($background_color == 'Gray'){ echo 'bg-gray-lighter flex-triangle'; } ?>">
	<div class="container">
		<div class="content-area">

			<?php if( have_rows('quick_nav') ): ?>
			   
			   <section class="quicklinkcont mt-lg">
				<?php while( have_rows('quick_nav') ): the_row(); ?>
				    <div class="quicklinkitem">
					    <div class="thumbnail">
					      <a href="<?php  the_sub_field('quick_link'); ?>" class="quicklinkimagecont text-center">
					      	<?php  the_sub_field('quick_icon'); ?>
					      </a>
					      <div class="text-center text-primary">
					        <h2 class="h3"><strong><?php  the_sub_field('quick_title'); ?></strong></h2>
					        <p><?php  the_sub_field('quick_description'); ?></p>
					      </div>
					    </div>
				    </div>		
				<?php endwhile; ?>
			   </section>

			<?php endif; ?>


		</div>
	</div>
</div>