<?php 
	$content          = get_sub_field('content');
	$background_color = get_sub_field('background_color');
?>

<div class="flexible-content pt-lg pb-lg <?php if($background_color == 'Gray'){ echo 'bg-gray-lighter flex-triangle'; } ?>">
	<div class="container">
		<div class="content-area">
			<?php echo $content; ?>
		</div>
	</div>
</div>