<?php 
	// $content          = get_sub_field('content');
	$background_color = get_sub_field('background_color');
?>

<div class="flexible-content pt-lg pb-lg <?php if($background_color == 'Gray'){ echo 'bg-gray-lighter flex-triangle'; } ?>">
	<div class="container">
		<div class="content-area">

				<div class="custom-shame">
				<?php while( have_rows('wells-flexible') ): the_row(); ?>
					<div class="well">
						<?php the_sub_field('cta-well_content'); ?>
					</div>	
				<?php endwhile; ?>
				</div>

		</div>
	</div>
</div>