<?php if( have_rows('block1') ):
	while ( have_rows('block1') ) : the_row();
		
		switch ( get_row_layout()) {

			// Alpha
			case 'block':
				get_template_part('inc/home-flex/cta-alpha');
			break;
			
			// quick_nav_flex
			case 'quick_nav_flex':
				get_template_part('inc/home-flex/cta-quick-nav');
			break;			
			
			// Wells
			case 'block_wells':
				get_template_part('inc/home-flex/cta-wells');
			break;	

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>