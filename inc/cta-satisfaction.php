<section class="cta-alpha pt-lg pb-lg">
	
	<div class="cta-img"><?php echo wp_get_attachment_image(1793, 'full'); ?></div>
	
	<div class="cta-body">
		<p class="lead"><span class="text-primary">Macbeth Roofing and Waterproofing</span> is a Vancouver roofing company that provides services for commercial roofing projects and residential roof repair, roof replacement and new construction.
		</p>
	</div>
</section> 