<?php $home_title = get_the_title( get_option('page_on_front') ); ?>

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars" aria-hidden="true"></i>
      </button>
    </div>
    
    <div id="main-navbar" class="collapse navbar-collapse">
      
      <?php 
      $currentPostId = $post->ID;

      // Loop until find the root parent post
      while(wp_get_post_parent_id( $currentPostId ) != FALSE){
        $currentPostId = wp_get_post_parent_id( $currentPostId );
      }
      //

      $rootPost = get_post( $currentPostId );

      if (!in_array($rootPost->post_name, array('commercial', 'residential'))){
        wp_nav_menu( array(
          'theme_location'    => 'primary-menu-general',
          'depth'             => 2,
          'menu_class'        => 'nav navbar-nav',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
        );
      }else if($rootPost->post_name == 'commercial'){
        wp_nav_menu( array(
          'theme_location'    => 'primary-menu-commercial',
          'depth'             => 2,
          'menu_class'        => 'nav navbar-nav',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
        );
      }else if($rootPost->post_name == 'residential'){
        wp_nav_menu( array(
          'theme_location'    => 'primary-menu-residential',
          'depth'             => 2,
          'menu_class'        => 'nav navbar-nav',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
        );
      }
      ?>

    </div>
</nav>
