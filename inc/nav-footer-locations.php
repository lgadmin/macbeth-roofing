<div class="site-content">
	
	<?php wp_nav_menu( 
		array(
			'theme_location' => 'footer-menu-locations',
			'container'      => false,
			'depth'          => 1,
			'menu_class'     => 'list-inline myinline-list'
		)
	); ?>
</div>
