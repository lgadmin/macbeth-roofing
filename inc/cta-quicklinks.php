<?php if( have_rows('quick_nav') ): ?>
   
   <section class="quicklinkcont mt-lg">
	<?php while( have_rows('quick_nav') ): the_row(); ?>
	    <div class="quicklinkitem">
		    <div class="thumbnail">
		      <a href="<?php  the_sub_field('quick_link'); ?>" class="quicklinkimagecont">
		      	<?php echo wp_get_attachment_image(get_sub_field('quick_image'), 'full'); ?>
		      </a>
		      <div class="caption">
		        <h2><?php  the_sub_field('quick_title'); ?></h2>
		        <p><?php  the_sub_field('quick_description'); ?></p>
		      </div>
		    </div>
	    </div>		
	<?php endwhile; ?>
   </section>

<?php endif; ?>
	   

