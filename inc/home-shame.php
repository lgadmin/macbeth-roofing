<?php if( have_rows('wells') ): ?>
	
	<div class="custom-shame">
		<?php while( have_rows('wells') ): the_row(); ?>
			<div class="well">
				<?php the_sub_field('well_content'); ?>
			</div>
		<?php endwhile; ?>	
	</div>

<?php endif; ?>


  
