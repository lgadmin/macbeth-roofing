<div class="site-content">
	
	<?php wp_nav_menu( array(
	  'theme_location'    => 'footer-menu-utility',
	  'container' => false,
	  'depth'             => 1,
	  'menu_class'        => 'list-inline myinline-list',
	  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	  'walker'            => new wp_bootstrap_navwalker())
	); ?>


	<div class="lg-sig">
		<?php  get_template_part("/inc/site-footer-longevity"); ?>
	</div>
	
</div>
