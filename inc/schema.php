<!-- FOR ADDRESS -->
<script type='application/ld+json'> 
{
    "@context": "http://schema.org",
	"@type": [
		"RoofingContractor"
	],
    "name": "Macbeth Roofing & Waterproofing",
    "description": "Contact Us",
    "address": [
        {
        "@type": "PostalAddress",
        "streetAddress": "12891 - 116 Ave",
        "addressLocality": "Surrey",
        "addressRegion": "BC",
        "postalCode": "V3R 2S5"
        },

        {
        "@type": "PostalAddress",
        "streetAddress": "1275 W 6th Ave",
        "addressLocality": "Vancouver",
        "addressRegion": "BC",
        "postalCode": "V6H 1A6"
        }
    ],
    "telephone":"(604) 593-1044",
    "image": "https://macbethroof.com/wp-content/uploads/2017/09/macbeth-logo-mission.jpg"
}
</script>

<!-- FOR OPENING TIME -->
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "OpeningHoursSpecification",
	"dayOfWeek": [
		"Monday",
        "Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
        "Saturday",
        "Sunday"
	],
	"opens": "00:00:00",
	"closes": "23:59:59"
}
</script>


<!-- FOR NAVBAR -->
<script type='application/ld+json'> 
{
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "@id": "main-navbar"
}
</script>

