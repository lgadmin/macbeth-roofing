<?php if ( has_post_thumbnail() && is_singular() ) : ?>
	<div class="featured-image">
		<div class="title-cont">
			<span class="featured-title"><?php echo get_the_title(); ?></span>
		</div>
		<div class="img-cont">
			<?php the_post_thumbnail(); ?>
		</div>
	</div>
<?php else : ?>
	<div class="featured-image">
		<div class="title-cont">
			<span class="featured-title">
				<?php 
					if ( is_home() || get_post_type() == 'post' ) {
						echo "Blog";
					} else{
						echo get_the_title(); 
					}
				?>
				</span>
		</div>
		<div class="img-cont bg-brand-primary">
			<div class="bg-brand-primary"></div>
		</div>
	</div>
<?php endif ?>