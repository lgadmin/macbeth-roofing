<?php $phone = get_field('company_phone', 'option'); ?>
<div class="bg-brand-primary pt-md pb-md">
	<section class="cta-request-quote text-center">
		<?php if (is_page('request-a-quote')): ?>
			<a class="h3 text-uppercase" href="tel:=+1<?php echo str_replace(["-", "–"], '', $phone) ?>"><span class="nobreak">Call Us Now </span><span style="color:white;text-decoration: underline;"><?php echo $phone; ?></span></a>
		<?php else : ?>
			<a href="/request-a-quote/" class="h3 text-uppercase">Request a <span>free quote</span></a> <a class="h3 text-uppercase" href="tel:=+1<?php echo $phone; ?>"><span class="nobreak"><?php echo $phone; ?></span></a>
		<?php endif ?>
	</section>
</div>
