<div class="slider-<?php echo $post->post_name; ?> slider-custom-post-type">
	<?php if( have_rows('slider') ): ?>
		<?php while( have_rows('slider') ): the_row(); 

		$image = get_sub_field('image');

		?>
		<div>
			<img src="<?php echo $image; ?>" alt="">
		</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
<div class="intro-block">
	<h2><?php the_field('intro-block-title') ?></h2>
	<?php the_field('intro-block-description') ?>
</div>


<!-- NAVIGATION ARROWS THROUGH POSTS -->


<div class='blog-post-arrows'>
    <?php previous_post_link( '%link', '<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>', TRUE, ' ', 'portfolio_category' ); ?> 
    <?php next_post_link( '%link', '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', TRUE, ' ', 'portfolio_category' ); ?> 
</div>

<!-- END -->

<script>
	// Slick Slider
	jQuery(document).ready(function(){
		jQuery('.slider-<?php echo $post->post_name; ?>').slick({
			infinite: true,
			dots: true,
			arrows: true,
			autoplay: true,
			infinite: true,
		});
	});
</script>

