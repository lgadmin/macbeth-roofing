<?php if( have_rows('vendors') ): ?>

   <ul class="vendors-list">
	<?php while( have_rows('vendors') ): the_row(); ?>
		<?php $image = wp_get_attachment_image_src(get_sub_field('vendor_logo'), 'full'); ?>
		<li><a href="<?php the_sub_field('vendor_link'); ?>">
			<?php echo wp_get_attachment_image(get_sub_field('vendor_logo'), 'full'); ?>
		</a></li>
	<?php endwhile; ?>
   </ul>

<?php endif; ?> 