<?php

// check if the flexible content field has rows of data
if( have_rows($block) ):
	$count = 0;
     // loop through the rows of data
    while ( have_rows($block) ) : the_row();
    	$count++;
        if( get_row_layout() == 'block' ):
        	$content = get_sub_field('content');
            $background_color = get_sub_field('background_color');
        ?>
		<div class="flexible-content pt-lg pb-lg <?php if($background_color == 'Gray'){ echo 'bg-gray-lighter'; } ?>">
			<div class="container">
				<div class="content-area">
					<?php echo $content; ?>
				</div>
			</div>
		</div>
        <?php
        endif;
    endwhile;
else :
    // no layouts found
endif;
?>