<div class="slidewrap">
	
	<section class="main-slider"> 
		
		<?php if( have_rows('slides') ): ?>
			<?php while( have_rows('slides') ): the_row(); ?>
				<div><?php echo wp_get_attachment_image(get_sub_field('slide_image'), 'full'); ?></div>
			<?php endwhile; ?>
		<?php endif; ?>

	</section>
	
	<div class="slider-content">
		<div class="container">
			
			<div class="slider-copy">
				<div class="xybox">
					<?php the_field('slider_copy'); ?>
				</div>
			</div>
			<div class="slider-form"><?php echo do_shortcode('[gravityform id="3" title="true" description="true"]'); ?></div>

		</div>
	</div>

</div>

