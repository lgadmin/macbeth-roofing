<ul class="blogfeed-list">
	<?php $blog_arg = array(
		'type'            => 'postbypost',
		'limit'           => 5,
		'format'          => 'html', 
		'show_post_count' => false,
		'echo'            => 1,
		'order'           => 'DESC',
		'post_type'       => 'post'
	);
	wp_get_archives( $blog_arg ); ?>
</ul>