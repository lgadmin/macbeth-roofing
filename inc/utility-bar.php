<div class="utility-bar">
	<div>
		
		<div class="social-media-cont">
			<ul class="social-media">
				<?php 
					$fb_enabled = get_field('facebook', 'option')['enable'];
					$fb_link = get_field('facebook', 'option')['url'];
					$fb_icon = get_field('facebook', 'option')['icon'];
					$tt_enabled = get_field('twitter', 'option')['enable'];
					$tt_link = get_field('twitter', 'option')['url'];
					$tt_icon = get_field('twitter', 'option')['icon'];
					$rss_enabled = get_field('rss', 'option')['enable'];
					$rss_link = get_field('rss', 'option')['url'];
					$rss_icon = get_field('rss', 'option')['icon'];
					$yt_enabled = get_field('youtube', 'option')['enable'];
					$yt_link = get_field('youtube', 'option')['url'];
					$yt_icon = get_field('youtube', 'option')['icon'];
					if($fb_enabled && $fb_link && $fb_icon){
						echo "<li>";
						echo "<a href='";
						echo $fb_link;
						echo "' target='_blank'>";
						echo $fb_icon;
						echo "</a>";
						echo "</li>";
					}
					if($tt_enabled && $tt_link && $tt_icon){
						echo "<li>";
						echo "<a href='";
						echo $tt_link;
						echo "' target='_blank'>";
						echo $tt_icon;
						echo "</a>";
						echo "</li>";
					}
					if($rss_enabled && $rss_link && $rss_icon){
						echo "<li>";
						echo "<a href='";
						echo $rss_link;
						echo "' target='_blank'>";
						echo $rss_icon;
						echo "</a>";
						echo "</li>";
					}
					if($yt_enabled && $yt_link && $yt_icon){
						echo "<li>";
						echo "<a href='";
						echo $yt_link;
						echo "' target='_blank'>";
						echo $yt_icon;
						echo "</a>";
						echo "</li>";
					}
				 ?>
			</ul>			
		</div>


		<div class="googletranslate"><?php echo do_shortcode('[google-translator]'); ?></div>

		<div class="call-us">

			<?php $phone = get_field('company_phone', 'option'); ?>
			<a href="tel:+1<?php echo $phone; ?>">
				<div>
					<i class="fa fa-phone" aria-hidden="true"></i> 24HR
				</div>
				<div>
					<?php echo $phone; ?>
				</div>
			</a>

		</div>
	</div>
</div>