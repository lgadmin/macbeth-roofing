<!-- Default Address Stuff -->
	<address>
	
		<span class="card-map-marker">
			<span>12891 – 116 Ave,</span><br>
			<span>Surrey</span>, <span>BC</span>&nbsp;<span>V3R&nbsp;2S5</span><br>
		</span>
		<br>
		<span class="card-map-marker">
			<span>1275 W 6th Ave,</span><br>
			<span>Vancouver</span>, <span>BC</span>&nbsp;<span>V6H&nbsp;1A6</span><br>
		</span>
		<br>
		<span class="card-map-marker">
			<span>#220 – 145 Chadwick Court,</span><br>
			<span>North Vancouver</span>, <span>BC</span>&nbsp;<span>V7M&nbsp;3K1</span><br>
		</span>
		<br>

		<?php 
			// Format Phone Numbers Function
			function format_phone_code($phone)
			{
				$phone = preg_replace("/[^0-9]/", "", $phone);
			 
				if(strlen($phone) == 7)
					return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
				elseif(strlen($phone) == 10)
					return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
				else
					return $phone;
			}

			$phone = get_field('company_phone', 'option'); 
			$cleanphone = str_replace('-', '', $phone);
			$phone = format_phone_code($cleanphone);

		?>
		
		<span class="card-map-phone">Phone: <a href="tel:+1<?php echo $cleanphone; ?>"><?php echo $phone; ?></a></span><br>
		<strong>Available 24/7</strong>

	</address>
 
