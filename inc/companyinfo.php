<div class="mb-lg"><?php the_custom_logo(); ?></div>

<p>Welcome to Macbeth Roofing. We are a well-respected Vancouver roofing company, specializing in both commercial and residential roofing. We provide roofing services including roof maintenance, roof repairs, re-roofs, and new construction on both residential and commercial buildings.</p>

<p class="small">&copy; Copyright <?php echo date("Y"); ?> MacBeth Roofing &amp; Waterproofing. All rights reserved. A division of Macbeth Roofing Corp.</p>