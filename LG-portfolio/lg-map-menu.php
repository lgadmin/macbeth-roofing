<?php
    add_action( 'admin_menu', 'register_cpt_menu_page');
    function register_cpt_menu_page() {
        add_menu_page(
            __( 'Read Me', 'cpt-menu' ),
            'LG-Portfolio',
            'manage_options',
            'cpt-settings',
            'cpt_settings_page',
            'dashicons-admin-media'
        );

        // Add Portfolio sub-menu
        add_submenu_page(
            'cpt-settings', 
            'Portfolio', 
            'Portfolio', 
            'manage_options', 
            'edit.php?post_type=portfolio'
        );

        add_submenu_page(
            'cpt-settings',
            'Types',
            'Category',
            'manage_options',
            'edit-tags.php?taxonomy=portfolio_category'
        );

        remove_submenu_page('cpt-settings','cpt-settings');
    }

    function cpt_settings_page() {
        include 'cpt-settings-page.php';
    }
    
?>