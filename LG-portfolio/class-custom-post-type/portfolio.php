<?php
 
	class Portfolio {
	    public function __construct() {
	        $portfolio = new Custom_Post_Type( 'portfolio', array(
	            'labels' => array(
	                'name' => __( 'Portfolio' ),
	                'singular_name' => __( 'Portfolio' )
	            ),
	            'public' => true,
	            'has_archive' => false,
	            'rewrite' => array('slug' => 'portfolio'),
	            'show_in_menu' => false
	        ) );

	        $portfolio -> add_taxonomy( 'portfolio_category',  array(
	            'label' => __( 'Type' ),
				'rewrite' => array( 'slug' => 'type', 'hierarchical' => true ),
				'hierarchical' => true,
	            'show_ui' => false,
	            'show_admin_column' => false
	        ) );

	        $this->portfolio = $portfolio;
	    }
	}
?>