// Slick Slider
(function($) {
  $(document).ready(function(){
    $('.main-slider').slick({
		lazyLoad: 'ondemand',
		dots: false,
		fade: false,
		arrows: false,
		cssEase: 'linear',
		infinite: true,
		autoplayspeed: 8000,
		speed: 800,
		slidesToShow: 1,
		autoplay: true,
		slidesToScroll: 1,
	});

	$('.request-a-quote-cta').on('click', function(){
		$('html, body').animate({
	        scrollTop: $("#gform_widget-3").offset().top
	    }, 2000);
	});
 });
}(jQuery));