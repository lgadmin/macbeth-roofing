<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package macbeth-roofing
 */

get_header(); ?>

	<div id="primary">
		<main id="main" class="site-main single-custom-post-type">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'inc/portfolio', get_post_type() );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
