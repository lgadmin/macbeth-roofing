<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package macbeth-roofing
 */

get_header(); ?>

	<!-- Quick Links Navigation -->
	<div class="container"> <?php  get_template_part('inc/cta-quicklinks') ?> </div>

	<!-- Flexible Content -->
	<?php  get_template_part('inc/home-flex/cta-flexible') ?>

	<!-- Satisfaction Badge CTA -->
	<div class="container"> <?php  get_template_part('inc/cta-satisfaction') ?> </div>

	<!-- Testimonials -->
	<div class="bgc-fixed-alpha pt-lg pb-lg">
		<div class="bg-overlay"></div>
		<div class="container">
			<div class="text-center text-primary" style="position: relative;"><i class="fa fa-5x fa-quote-left" aria-hidden="true"></i></div>
			<?php echo do_shortcode('[hms_testimonials_rotating template="1" order="random" direction="ASC"  word_limit=50]'); ?>
		</div>
	</div>

	<!-- Vendor Logos -->
	<div class="container text-center"> <?php  get_template_part('inc/cta-vendorlogolinks') ?> </div>

<?php get_footer(); ?>
