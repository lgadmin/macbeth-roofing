<?php
/* Template Name: Page Portfolio Template */ 


get_header(); ?>


	<div id="primary">
		<main id="main" class="container single-custom-post-type">
		
		<?php get_template_part('inc/breadcrumb') ?>

			<?php 
			$currentPostId = $post->ID;

		    // Loop until find the root parent post
		    while(wp_get_post_parent_id( $currentPostId ) != FALSE){
		      $currentPostId = wp_get_post_parent_id( $currentPostId );
		    }
		    //

      		$rootPost = get_post( $currentPostId );

			$loop = new WP_Query( array( 
				'post_type' => 'portfolio', 
				'tax_query' => array(
			        array (
			            'taxonomy' => 'portfolio_category',
			            'field' => 'slug',
			            'terms' => $rootPost->post_name,
			        )
			    ), 
			    'posts_per_page' => 100 
			) ); ?>

			<ul class="porfolio-archieve">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<li>
						<?php if( have_rows('slider') ){ ?>
							<?php while( have_rows('slider') ): the_row(); 

								$image = get_sub_field('image');
							 endwhile; ?>
						<?php }else{ $image = "";} ?>
						<div>
							<a href="<?php echo get_permalink(); ?>"><img src="<?php echo $image; ?>" alt=""></a>
						</div>
					    <?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>

					    <div class="entry-content">
					        <?php the_content(); ?>
					    </div>
					</li>
				<?php endwhile; ?>
			</ul>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
